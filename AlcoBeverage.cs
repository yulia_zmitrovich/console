﻿using System;
using System.Collections.Generic;

namespace Shop
{
	class AlcoBeverage: Product
	{
		public String classification;
		public String strength;
		public static List<String> alcoList = new List<string>(){"wine", "strong alcohol", "beer", "liqueur"};
		public AlcoBeverage()
	  	{
	  	}
		
		public override String ToString()
	  	{
	    	return name + "\t" + purchasePrice + "\t" + classification + "\t" + volume + "\t" + strength + "\t" + availability + "\n\n";
	  	}
	}
}