﻿using System;
using System.Collections.Generic;

namespace Shop
{
	class Beverage: Product
	{
		public String group;
		public String composition;
		public static List<String> beverageList = new List<string>(){"mineral water", "juice", "beverage"};
		public Beverage()
	  	{
	  	}
		
		public override String ToString()
	  	{
	    	return name + "\t" + purchasePrice + group + "\t" + volume + "\n" + composition + "\t" + availability + "\n\n";
	  	}
	}
}