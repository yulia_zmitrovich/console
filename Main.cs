﻿using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using Shop;

class ReadingFiles 
{
  public static void Main() 
  {
  	List<Product> products = LoadProducts();
  	PrintProducts(products);  
  	
	Random rand = new Random(); 
  	Product purchasedProduct = new Product();
  	purchasedProduct = products.ElementAt(rand.Next(1, 8));
  	
  	Double extraCharge = 0;
	DateTime currentDateTime = DateTime.Now;	
	DayOfWeek dayOfWeek = currentDateTime.DayOfWeek;	
	if(dayOfWeek == DayOfWeek.Saturday || dayOfWeek == DayOfWeek.Sunday)
		extraCharge = purchasedProduct.purchasePrice + (purchasedProduct.purchasePrice * 15)/100;
	if(currentDateTime.Hour > 18 && currentDateTime.Hour < 20) 
		extraCharge = purchasedProduct.purchasePrice + (purchasedProduct.purchasePrice * 8)/100;
	else
		extraCharge = purchasedProduct.purchasePrice + (purchasedProduct.purchasePrice * 10)/100;
			
	//Console.WriteLine(purchasedProduct.purchasePrice);
  	
	Console.ReadKey();  	
	
  }
  
  static void PrintProducts(List<Product> products)
  {
  	foreach (Product prod in products)
		Console.WriteLine(prod);    
  }
  
  static List<Product> LoadProducts()
  {
  	var reader = new StreamReader(File.OpenRead(@"Products.csv"));
	List<Product> products = new List<Product>();  	
	while (!reader.EndOfStream)
	{
		var line = reader.ReadLine();
		var values = line.Split('\n'); 
		for (int i = 0; i < values.Length; i++)
		{
			String[] tokens = values[i].Split(',');
           	String searchValue = tokens[2];
           	Product p = new Product();
           	p = GetProductBySearchValue(searchValue, tokens);
           	products.Add(p);      
           }
	}
	return products;
  }
  
  static Product GetProductBySearchValue(String searchValue, String[] tokens)
  {
	searchValue = searchValue.Substring(2, searchValue.Length-3);
	if(AlcoBeverage.alcoList.Contains(searchValue))
	{
		AlcoBeverage a = new AlcoBeverage();
		a.name = tokens[0];
		a.purchasePrice = Double.Parse(tokens[1].Substring(1, tokens[1].Length-1));
		a.classification = tokens[2];
		a.volume = tokens[3]; 
		a.strength = tokens[4]; 
		a.availability = tokens[5];
		return a;
	}
  	if(Beverage.beverageList.Contains(searchValue))
  	{
		Beverage b = new Beverage();
		b.name = tokens[0];
		b.purchasePrice = Double.Parse(tokens[1].Substring(1, tokens[1].Length-1));
		b.group = tokens[2];
		b.volume = tokens[3];  
		b.composition = tokens[4]; 
		b.availability = tokens[5];
		return b;
  	}
  	return null;
  }
}
